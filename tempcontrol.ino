#include "avr/wdt.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include <RotaryEncoder.h>
#include "EEPROM.h"
#include <math.h>

volatile int8_t menu_page = 0;

unsigned long debouncing_time = 300; //Debouncing Time in Milliseconds
volatile unsigned long last_millis = 0;

#define _debnc_begin  if ((millis() - last_millis) >= debouncing_time)
#define _debnc_end     last_millis = millis();
#define heater_ON_STATE LOW

LiquidCrystal_I2C lcd(0x27, 16, 2);
RotaryEncoder encoder(A3, A2);

uint8_t buttonPin = 2;

uint8_t intTempPin = A6;
uint8_t extTempPin = A7;

uint8_t heaterPin1 = 5;
uint8_t heaterPin2 = 6;                  ;
volatile uint8_t btnPressed = 0;
float getTemp(uint8_t pin);
float R1 = 10000;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
float vals[6] = {0, 20, 0, 50, getTemp(intTempPin), getTemp(extTempPin)};

#define intTempMin vals[0]
#define  intTempMax vals[1]
#define  extTempMin vals[2]
#define extTempMax vals[3]
#define  intTempCurrent vals[4]
#define extTempCurrent vals[5]

void heaterOn();
void heaterOff();
void btnFunc();
void drawMain();
void drawIntMin();
void drawIntMax();
void drawExtMin();
void drawExtMax();
void tempAlgo();
bool errorMode = true;
bool selected = false, conf_mode = false, store = false;
bool clearNeeded = false;
bool timerStop  = false;
int pos = 0;
bool backlight_needed = false;
unsigned long period = 0, period1 = 10000;
//#define FAC_DEF

void setup()
{
  pinMode(heaterPin1, OUTPUT);
  pinMode(heaterPin2 , OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(buttonPin), btnFunc, FALLING);
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  PCICR |= (1 << PCIE1);    // This enables Pin Change Interrupt 1 that covers the Analog input pins or Port C.
  PCMSK1 |= (1 << PCINT10) | (1 << PCINT11);  // This ena
  Serial.begin(9600);

#ifndef FAC_DEF
  intTempMin =  (float)(EEPROM.read(0) << 8 | EEPROM.read(1)) / 10;
  intTempMax = (float)(EEPROM.read(2) << 8 | EEPROM.read(3)) / 10;
#endif
  wdt_enable(WDTO_4S);
}

void heaterOn() {
  digitalWrite(heaterPin1, heater_ON_STATE);
  digitalWrite(heaterPin2, heater_ON_STATE);
}
void heaterOff() {
  digitalWrite(heaterPin1, !heater_ON_STATE);
  digitalWrite(heaterPin2, !heater_ON_STATE);
}
float oldItc = 0;
float oldEtc = 0;
ISR(PCINT1_vect) {
  encoder.tick(); // just call tick() to check the state.
}

void loop()
{

  if (millis() - period >= 15000) {
    menu_page = 0;
   // lcd.noBacklight();
  //  backlight_needed = true;
  }
  if (millis() - period1 >= 500) {
    intTempCurrent = getTemp(intTempPin);
    extTempCurrent = getTemp(extTempPin);
    period1 = millis();
  }


  if (menu_page != 0) {
    uint16_t sval = 0;
    int newPos = encoder.getPosition();
    if (pos != newPos) {
      if (pos > newPos) {
        period = millis();
        vals[menu_page - 1] -= 0.1;
        if (vals[menu_page - 1] < 0)vals[menu_page - 1] = 0;

      }
      else {
        period = millis();
        vals[menu_page - 1] += 0.1;

      }
      sval = vals[menu_page - 1] * 10;
      EEPROM.update((menu_page - 1) * 2, (uint8_t)(sval >> 8)); //2 4 6 8
      EEPROM.update((menu_page - 1) * 2 + 1, (uint8_t)(sval & 0xff)); //3 5 7 9
      pos = newPos;
    }
  }


  switch (menu_page) {
    case 0: drawMain(); tempAlgo(); break;
    case 1: drawIntMin(); break;
    case 2: drawIntMax(); break;

  }
  wdt_reset();
}
float getTemp(uint8_t tempPin) {
  float logR2, R2, T, Tc, Tf;
  int Vo = analogRead(tempPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
  Tc = T - 273.15;
  if (Tc < -200)errorMode = true; else errorMode = false;

  /*if(tempPin==intTempPin)
    Tc = filter_analog1(Tc);
    if(tempPin==extTempPin)
    Tc = filter_analog1(Tc);
  */

  return Tc;
}
void btnFunc() {
  _debnc_begin{
//if(backlight_needed){lcd.backlight(); backlight_needed = false;}
    period = millis();
    clearNeeded = true;
    menu_page++;
    if (menu_page > 2)menu_page = 0;
    _debnc_end}
}

void drawMain() {
  char buf1[15] = {0};
  if (clearNeeded) {
    lcd.clear();
    clearNeeded = false;
  }
  lcd.setCursor(0, 0);
  lcd.print("Int. temp: ");
  dtostrf(intTempCurrent, 5, 1, buf1);
  lcd.print(buf1);
  lcd.setCursor(0, 1);
  lcd.print("Ext. temp: ");
  dtostrf(extTempCurrent, 5, 1, buf1);
  lcd.print(buf1);
}
void drawIntMin() {
  if (clearNeeded) {
    lcd.clear();
    clearNeeded = false;
  }
  char buf1[15] = {0};
  lcd.setCursor(0, 0);
  lcd.print("Min. int. temp: ");
  dtostrf(intTempMin, 5, 1, buf1);
  lcd.setCursor(0, 1);
  lcd.print(buf1); lcd.print(" C");

}
void drawIntMax() {
  if (clearNeeded) {
    lcd.clear();
    clearNeeded = false;
  }
  char buf1[15] = {0};
  lcd.setCursor(0, 0);
  lcd.print("Max. int. temp: ");
  dtostrf(intTempMax, 5, 1, buf1);
  lcd.setCursor(0, 1);
  lcd.print(buf1); lcd.print(" C");
}

/*float filter_analog1(float input)
  {
    static float result1 = 0;
    float alfa = 0.7;
    result1 = result1 * alfa + input * (1 - alfa);
    return result1;
  }

  float filter_analog2(float input)
  {
    static float result2 = 0;
    float alfa = 0.7;
    result2 = result2 * alfa + input * (1 - alfa);
    return result2;
  }
*/
void drawExtMin() {
  if (clearNeeded) {
    lcd.clear();
    clearNeeded = false;
  }
  char buf1[15] = {0};
  lcd.setCursor(0, 0);
  lcd.print("Min. ext. temp: ");
  dtostrf(extTempMin, 5, 1, buf1);
  lcd.setCursor(0, 1);
  lcd.print(buf1); lcd.print(" C");
}
void drawExtMax() {
  if (clearNeeded) {
    lcd.clear();
    clearNeeded = false;
  }
  char buf1[15] = {0};
  lcd.setCursor(0, 0);
  lcd.print("Max. ext. temp: ");
  dtostrf(extTempMax, 5, 1, buf1);
  lcd.setCursor(0, 1);
  lcd.print(buf1); lcd.print(" C");
}
void  tempAlgo() {
  /*if(intTempMax>extTempCurrent+1){
    if(intTempCurrent > intTempMax){
    heaterOn();
    }
    if(intTempMin>extTempCurrent+1){
    if(intTempCurrent<=intTempMin)heaterOff();
    }
    else{
      if(intTempCurrent<=extTempCurrent)heaterOff();
      }

    }
    else heaterOff();*/

  if (errorMode)
  {
    heaterOff();
  }
  else {

    if (intTempMax > extTempCurrent + 1) {
      if (intTempCurrent > intTempMax) {
        heaterOff();
      }
      else {

        if (intTempMin > extTempCurrent + 1) {
          if (intTempCurrent <= intTempMin) {
            heaterOn();
          }
          else {
            //DO NOTHING
          }
        }
        else {
          if (intTempCurrent <= extTempCurrent + 1) {
            heaterOn();
          }
          else {
            //DO NOTHING
          }
        }

      }
    }
    else {
      heaterOff();
    }

  }
}
